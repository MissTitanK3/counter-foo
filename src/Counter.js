import React from "react";
import useCounter from "./useCounter";

export const Counter = () => {
  const { increment, decrement, reset, count } = useCounter()

  return (
    <>
      <h1>{count}</h1>
      <div>
        <button onClick={increment}>Increment</button>
        <button onClick={decrement}>Decrement</button>
        <button onClick={reset}>Set to Zero</button>

      </div>
    </>
  )

}
